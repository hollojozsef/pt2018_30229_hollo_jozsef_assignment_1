package PT2018.demo.Project1;



public class Monom {
	public int coef = 0;
	public int putere = 0;
	public int getCoef() {
		return coef;
	}

	public void setCoef(int coef) {
		this.coef = coef;
	}

	public int getPutere() {
		return putere;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}

	public Monom(int coef, int putere) {
		this.coef = coef;
		this.putere = putere;
	}
	public void adunaMonom(Monom m) {
		if(this.putere==m.getPutere()) {
			this.coef+=m.getCoef();
		}
	}
	public void scadeMonom(Monom m) {
		if(this.putere==m.getPutere()) {
			this.coef-=m.getCoef();
		}
	}
	public void inmultesteMonom(Monom m) {
		this.putere+=m.getPutere();
		this.coef=this.coef*m.getCoef();
	}
	public String toString() {
		if(putere==0&&coef==0)
			return "";
		if(putere==0)
			if(coef>0)
			return "+"+Integer.toString(coef);
			else
				return Integer.toString(coef);	
		else
			if(coef==0)
				return "";
			else 
				if(coef>0)
					return "+"+coef+"x^"+putere;
				else
					return coef+"x^"+putere;

		
	}
}


