package PT2018.demo.Project1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Polinom {

	ArrayList<Monom> polinom;

	public Polinom(String intrare) {
		polinom = Polinom1(intrare);
	}

	public ArrayList<Monom> Polinom1(String intrare) {
		ArrayList<Monom> polinom2 = new ArrayList<Monom>();
		intrare = intrare.replaceAll("[^+-?0-9]+", " ");
		String[] splitStr11 = intrare.split("\\s+");
		for (int i = 0; i < splitStr11.length; i += 2) {
			String next1 = splitStr11[i];
			String next2 = splitStr11[i + 1];
			int pol1 = Integer.parseInt(next1);
			int pol2 = Integer.parseInt(next2);
			Monom m = new Monom(pol1, pol2);
			polinom2.add(m);
		}
		return polinom2;

	}

	public void setPolinom(ArrayList<Monom> m) {
		polinom = m;
	}

	public void setMonom(ArrayList<Monom> a) {
		this.polinom = a;
	}

	public void addMonom(Monom m) {
		polinom.add(m);
	}

	public String toString() {
		String strOutput = " ";
		// for(int i = 0; i < polinom.size(); i++){

		for (Iterator<Monom> it1 = polinom.iterator(); it1.hasNext();) {
			Monom m1 = it1.next();
			strOutput += m1.toString();

		}
		// strOutput=strOutput.replaceAll("2", " ");
		return strOutput;

	}

	public ArrayList<Monom> getPolinom() {
		return polinom;
	}

	public Monom getPoz(int i) {
		return polinom.get(i);
	}

	public static ArrayList<Monom> aduna(Polinom polinom1, Polinom polinom2) {
		ArrayList<Monom> rezultat = new ArrayList<Monom>();
		int i = 0, j = 0;
		while (i < polinom1.size() && j < polinom2.size()) {
			Monom m1 = polinom1.getPoz(i);
			Monom m2 = polinom2.getPoz(j);
			if (m1.getPutere() == m2.getPutere()) {
				int putere, coeficient;
				putere = m1.getPutere();
				coeficient = m1.getCoef() + m2.getCoef();
				Monom m3 = new Monom(coeficient, putere);
				rezultat.add(m3);
				i++;
				j++;
			} else if (m1.getPutere() > m2.getPutere()) {
				rezultat.add(m1);
				i++;
			} else if (m1.getPutere() < m2.getPutere()) {
				rezultat.add(m2);
				j++;
			}
		}
		while (i < polinom1.size()) {
			Monom m3 = polinom1.getPoz(i);
			rezultat.add(m3);
			i++;
		}
		while (j < polinom2.size()) {
			Monom m4 = polinom2.getPoz(j);
			rezultat.add(m4);
			j++;
		}
		return rezultat;
	}

	private int size() {
		return polinom.size();
	}

	public static ArrayList<Monom> scade(Polinom polinom1, Polinom polinom2) {
		ArrayList<Monom> rezultat = new ArrayList<Monom>();
		int i = 0, j = 0;
		while (i < polinom1.size() && j < polinom2.size()) {
			Monom m1 = polinom1.getPoz(i);
			Monom m2 = polinom2.getPoz(j);
			if (m1.getPutere() == m2.getPutere()) {
				int putere, coeficient;
				putere = m1.getPutere();
				coeficient = m1.getCoef() - m2.getCoef();
				Monom m3 = new Monom(coeficient, putere);
				rezultat.add(m3);
				i++;
				j++;
			} else if (m1.getPutere() > m2.getPutere()) {
				rezultat.add(m1);
				i++;
			} else if (m1.getPutere() < m2.getPutere()) {
				m2.setCoef(-m2.getCoef());
				rezultat.add(m2);
				j++;
			}
		}
		while (i < polinom1.size()) {
			Monom m3 = polinom1.getPoz(i);
			rezultat.add(m3);
			i++;
		}
		while (j < polinom2.size()) {
			Monom m4 = polinom2.getPoz(j);
			m4.setCoef(-m4.getCoef());
			rezultat.add(m4);
			j++;
		}
		return rezultat;
	}

	public static ArrayList<Monom> inmultire(Polinom polinom1, Polinom polinom2) {
		ArrayList<Monom> rezultat = new ArrayList<Monom>();
		ArrayList<Monom> rez = new ArrayList<Monom>();
		int i = 0, j = 0;
		while (i < polinom1.size()) {
			j = 0;
			while (j < polinom2.size()) {
				Monom m1 = polinom1.getPoz(i);
				Monom m2 = polinom2.getPoz(j);
				Monom m3 = new Monom(m1.getCoef() * m2.getCoef(), m1.getPutere() + m2.getPutere());
				rezultat.add(m3);
				j++;
			}
			i++;
		}

		for (i = 0; i < rezultat.size(); i++) {
			Monom m4 = rezultat.get(i);

			for (j = i + 1; j < rezultat.size(); j++) {
				Monom m5 = rezultat.get(j);
				if (m4.getPutere() == m5.getPutere()) {
					// Monom m6=new Monom(m4.getCoef()+m5.getCoef(),m4.getPutere());
					m4.adunaMonom(m5);
					m5.setCoef(0);
					m5.setPutere(0);
				}

			}
			rez.add(m4);
		}

		return rez;
	}

	public static ArrayList<Monom> derivare(Polinom polinom1) {
		ArrayList<Monom> rezultat = new ArrayList<Monom>();
		int i = 0;
		for (i = 0; i < polinom1.size(); i++) {
			Monom m4 = polinom1.getPoz(i);
			m4.setCoef(m4.getCoef() * m4.getPutere());
			m4.setPutere(m4.getPutere() - 1);
			rezultat.add(m4);

		}
		return rezultat;
	}

	public String integrare() {
		String rezultat = "";
		int i = 0;
		for (i = 0; i < polinom.size(); i++) {
			Monom m4 = polinom.get(i);
			String a = Integer.toString(m4.getCoef());
			String b = Integer.toString(m4.getPutere() + 1);
			if (m4.getCoef() > 0)
				rezultat += "+" + a + "/" + b + "x^" + b;
			else
				rezultat += a + "/" + b + "x^" + b;

		}
		return rezultat;
	}

	public void ordonare() {
		for (int i = 0; i < polinom.size(); i++) {
			Monom m1 = polinom.get(i);
			for (int j = 0; j < polinom.size(); j++) {
				Monom m2 = polinom.get(j);
				if (m1.getPutere() > m2.getPutere())
					Collections.swap(polinom, i, j);
			}
		}
	}

	/*
	 * public static ArrayList<Polinom> impartire(Polinom polinom1,Polinom
	 * polinom2){
	 * 
	 * ArrayList<Polinom> rezultat=new ArrayList<Polinom>(); Polinom p=new
	 * Polinom("0x^0"); Polinom r=new Polinom("0x^0"); Polinom q=new
	 * Polinom("0x^0"); ArrayList<Monom> rest=new ArrayList<Monom>();
	 * ArrayList<Monom> inter=new ArrayList<Monom>(); ArrayList<Monom> inter2=new
	 * ArrayList<Monom>(); Monom m2=polinom2.getPoz(0); Monom m1=polinom1.getPoz(0);
	 * if(m1.getPutere()<m2.getPutere()) { Monom m4=new Monom(0,0); r.addMonom(m4);
	 * rezultat.add(r); return rezultat; } for(int i=1;i<polinom1.size();i++) {
	 * m1=polinom1.getPoz(i); Monom m3=new
	 * Monom(m1.getCoef()/m2.getCoef(),m1.getPutere()-m2.getPutere());
	 * p.addMonom(m3); inter=inmultire(p,polinom2); q.setPolinom(inter);
	 * inter2=scade(polinom2,q); if(m3.getPutere()<m2.getPutere()) {
	 * rezultat.add(p); for(int j=i;j<polinom1.size();i++) {
	 * r.addMonom(polinom2.getPoz(j)); } } rezultat.add(r); return rezultat;
	 * 
	 * }
	 * 
	 * 
	 * 
	 * return rezultat;
	 * 
	 * }
	 */
}
