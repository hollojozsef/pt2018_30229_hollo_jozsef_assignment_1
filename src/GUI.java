package PT2018.demo.Project1;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class GUI extends JFrame implements ActionListener {
	private JLabel enterPolinom, enterPolinom2, suma, produs, impartire, integrare1, derivare1, integrare2,
			derivare2;
	private JTextField nume, nume2;
	private static JTextField rezSuma;
	private JTextField rezProd;
	private JTextField rezDif;
	private JTextField rezInt1;
	private JTextField rezInt2;
	private JTextField rezDer1;
	private JTextField rezDer2;
	private JButton click;
	public String polinom1 = "";
	public String polinom2 = "";

	public GUI() {
		declarari();
		setareLimite();
		setare();
		adaugare();
	}

	public void declarari() {

		setLayout(null);
		setSize(800, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		enterPolinom = new JLabel("Polinomul I");
		enterPolinom2 = new JLabel("Polinomul II");
		suma = new JLabel("Suma :");
		produs = new JLabel("Produsul :");
		impartire = new JLabel("Diferenta:");
		integrare1 = new JLabel("Integrare polinom I :");
		derivare1 = new JLabel("Derivare polinom I:");
		integrare2 = new JLabel("Integrare polinom II:");
		derivare2 = new JLabel("Derivare polinom II:");
		click = new JButton("Calculeaza");
		nume = new JTextField();
		nume2 = new JTextField();
		rezSuma = new JTextField();
		rezProd = new JTextField();
		rezDif = new JTextField();
		rezInt1 = new JTextField();
		rezInt2 = new JTextField();
		rezDer1 = new JTextField();
		rezDer2 = new JTextField();
	}

	public void setareLimite() {
		enterPolinom.setBounds(30, 30, 120, 30);
		enterPolinom2.setBounds(570, 30, 120, 30);
		nume.setBounds(80, 60, 130, 30);
		nume2.setBounds(580, 60, 130, 30);
		click.setBounds(350, 100, 100, 30);
		suma.setBounds(30, 150, 100, 30);
		rezSuma.setBounds(30, 200, 200, 30);
		produs.setBounds(30, 250, 100, 30);
		rezProd.setBounds(30, 300, 200, 30);
		integrare1.setBounds(30, 350, 120, 30);
		rezInt1.setBounds(30, 400, 200, 30);
		derivare1.setBounds(30, 450, 120, 30);
		rezDer1.setBounds(30, 500, 200, 30);
		impartire.setBounds(570, 250, 100, 30);
		rezDif.setBounds(570, 300, 200, 30);
		integrare2.setBounds(570, 350, 200, 30);
		rezInt2.setBounds(570, 400, 200, 30);
		derivare2.setBounds(570, 450, 200, 30);
		rezDer2.setBounds(570, 500, 200, 30);

	}

	public void setare() {
		rezSuma.setEditable(false);
		rezProd.setEditable(false);
		rezDif.setEditable(false);
		rezInt1.setEditable(false);
		rezInt2.setEditable(false);
		rezDer1.setEditable(false);
		rezDer2.setEditable(false);
		click.addActionListener(this);
	}

	public void adaugare() {
		add(rezInt1);
		add(rezInt2);
		add(rezDer1);
		add(rezDer2);
		add(integrare1);
		add(integrare2);
		add(derivare1);
		add(derivare2);
		add(rezDif);
		add(rezProd);
		add(rezSuma);
		add(click);
		add(nume);
		add(nume2);
		add(enterPolinom);
		add(enterPolinom2);
		add(suma);
		add(produs);
		add(impartire);
	}

	public String getFirstPolinom() {
		return nume.getText();

	}

	public String getSecondPolinom() {
		return nume2.getText();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == click) {
			Polinom p1 = new Polinom(getFirstPolinom());
			Polinom p2 = new Polinom(getSecondPolinom());
			Polinom p3 = new Polinom(getFirstPolinom());
			Polinom p4 = new Polinom(getSecondPolinom());
			p1.ordonare();
			p2.ordonare();
			p3.ordonare();
			p4.ordonare();
			Polinom suma = new Polinom("0x^0");
			suma.setMonom(Polinom.aduna(p1, p2));
			rezSuma.setText(suma.toString());
			Polinom prod = new Polinom("0x^0");
			prod.setMonom(Polinom.inmultire(p1, p2));
			prod.ordonare();
			rezProd.setText(prod.toString());
			Polinom der = new Polinom("0x^0");
			der.setMonom(Polinom.derivare(p1));
			rezDer1.setText(der.toString());
			Polinom der2 = new Polinom("0x^0");
			der2.setMonom(Polinom.derivare(p2));
			rezDer2.setText(der2.toString());
			String s1 = p3.integrare();
			rezInt1.setText(s1);
			String s2 = p4.integrare();
			rezInt2.setText(s2);
			Polinom dif = new Polinom("0x^0");
			dif.setMonom(Polinom.scade(p3, p4));
			rezDif.setText(dif.toString());
		}

	}

	public static void main(String[] args) {
		GUI a = new GUI();
		a.setVisible(true);
	}

}
